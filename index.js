const todos = [
  {
    userId: 1,
    id: 1,
    title: "delectus aut autem",
    completed: false,
  },
  {
    userId: 1,
    id: 2,
    title: "quis ut nam facilis et officia qui",
    completed: false,
  },
  {
    userId: 1,
    id: 3,
    title: "fugiat veniam minus",
    completed: false,
  },
  {
    userId: 1,
    id: 4,
    title: "et porro tempora",
    completed: true,
  },
  {
    userId: 1,
    id: 5,
    title: "laboriosam mollitia et enim quasi adipisci quia provident illum",
    completed: false,
  },
  {
    userId: 1,
    id: 6,
    title: "qui ullam ratione quibusdam voluptatem quia omnis",
    completed: false,
  },
  {
    userId: 1,
    id: 7,
    title: "illo expedita consequatur quia in",
    completed: false,
  },
  {
    userId: 1,
    id: 8,
    title: "quo adipisci enim quam ut ab",
    completed: true,
  },
  {
    userId: 1,
    id: 9,
    title: "molestiae perspiciatis ipsa",
    completed: false,
  },
  {
    userId: 1,
    id: 10,
    title: "illo est ratione doloremque quia maiores aut",
    completed: true,
  },
];

const fetchTodos = (throwError, callback) => {
  setTimeout(() => {
    if (throwError) {
      callback("An error has occured");
    } else {
      callback(undefined, todos);
    }
  }, 2000);
};

fetchTodos(false, (error, data) => {
  if (error) {
    console.error(error);
  }
  console.log(data);
});

const fetchTodosPromise = (throwError) =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      if (throwError) {
        reject("Network response was not ok");
      }
      resolve(todos);
    }, 2000);
  });

fetchTodosPromise(true)
  .then((todos) => {
    console.log(todos);
  })
  .catch((err) => console.error(err));
